package edu.uoc.android.restservice.ui.enter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


import edu.uoc.android.restservice.R;
import edu.uoc.android.restservice.rest.adapter.GitHubAdapter;
import edu.uoc.android.restservice.rest.model.Owner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import edu.uoc.android.restservice.rest.model.Owner;

public class InfoUserActivity extends AppCompatActivity{





    // Se crea una lista
    private ArrayList<Owner> listaFollowers = new ArrayList<>();
    RecyclerView recyclerViewFollowers;

    TextView textViewRepositories, textViewFollowing;
    ImageView imageViewProfile;

    ProgressBar progressBar;
    String loginName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_user);

        //obtenemos los valores de la variable intent

        Intent intent = getIntent();
        loginName = intent.getExtras().getString("user");

        labelFollowing = (TextView)findViewById(R.id.labelFollowing);
        labelRepositories = (TextView) findViewById(R.id.labelRepositories);
        labelFollowers = (TextView) findViewById(R.id.labelFollowers);

        textViewFollowing = findViewById(R.id.textViewFollowing);
        textViewRepositories = findViewById(R.id.textViewRepositories);
        imageViewProfile = (ImageView) findViewById(R.id.imageViewProfile);

        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        recyclerViewFollowers = (RecyclerView)findViewById(R.id.recyclerViewFollowers);
        recyclerViewFollowers.setLayoutManager(new LinearLayoutManager(this));


       initProgressBar();
        mostrarDatosBasicos(loginName);

      }

    TextView labelFollowing, labelRepositories, labelFollowers;

// Ocultar los recursos mientras Rtrofit consulta los datos
    private void initProgressBar()
    {
        progressBar.setVisibility(View.VISIBLE);
        textViewFollowing.setVisibility(View.INVISIBLE);
        textViewRepositories.setVisibility(View.INVISIBLE);
        imageViewProfile.setVisibility(View.INVISIBLE);
        recyclerViewFollowers.setVisibility(View.INVISIBLE);
        labelFollowing.setVisibility(View.INVISIBLE);
        labelRepositories.setVisibility(View.INVISIBLE);
        labelFollowers.setVisibility(View.INVISIBLE);
    }

// mostrar los recursos despues q retrofit halla obtenido los datos
    private void FinProgressBar()
    {
        progressBar.setVisibility(View.GONE);
        textViewFollowing.setVisibility(View.VISIBLE);
        textViewRepositories.setVisibility(View.VISIBLE);
        imageViewProfile.setVisibility(View.VISIBLE);
        recyclerViewFollowers.setVisibility(View.VISIBLE);
        labelFollowing.setVisibility(View.VISIBLE);
        labelRepositories.setVisibility(View.VISIBLE);
        labelFollowers.setVisibility(View.VISIBLE);
    }


// cagar imagen de la URL del avatar de git
    private void mostarimagen(String url){
        Picasso.get()
                .load(url)
                .error(R.mipmap.ic_launcher)
                .fit()
                .centerInside()
                .into(imageViewProfile);

    }




// metodo retroif muestra datos del usuario git
    private void mostrarDatosBasicos(String loginNameLista){
        GitHubAdapter adapter = new GitHubAdapter();
        Call<Owner> call = adapter.getOwner(loginNameLista);

        call.enqueue(new Callback<Owner>() {
            @Override
            public void onResponse(Call<Owner> call, Response<Owner> response) {

                if (response.isSuccessful() && response.body()!=null){
// lo que uno va a mostrar

                    Owner owner = response.body();
                    textViewRepositories.setText("Nombre:" + owner.getName());
                    textViewFollowing.setText("ID:" + owner.getId());
                    labelFollowers.setText("Company:" + owner.getCompany ());
                    labelRepositories.setText("Following: " + owner.getFollowing());
                    labelFollowing.setText("Followers: " + owner.getFollowers());

                    String url = owner.getAvatarUrl();
                    mostarimagen (url);
                    mostrarlista(loginName);


                }else{
                    if (response.body()==null){
                        Toast.makeText(getBaseContext(),"No se encontraron resultados" ,Toast.LENGTH_SHORT).show();

                    }

                }


            }

            @Override
            public void onFailure(Call<Owner> call, Throwable t) {
                Toast.makeText(getBaseContext(),"ERROR: "+ t.getLocalizedMessage() ,Toast.LENGTH_SHORT).show();
                FinProgressBar();
            }

        });

    }



// muestra la lista de seguidores

    private void mostrarlista(String loginName){

        GitHubAdapter adapter = new GitHubAdapter();
        Call<List<Owner>> call2 = adapter.getOwnerFollowers(loginName);


        call2.enqueue(new Callback<List<Owner>>() {
            @Override
            public void onResponse(Call<List<Owner>> call, Response<List<Owner>> response) {

                if (response.isSuccessful() && response.body()!=null) {
                    listaFollowers = new ArrayList<>(response.body());
                    AdaptadorFollowers dataAdapter=new AdaptadorFollowers(listaFollowers, InfoUserActivity.this);
                    recyclerViewFollowers.setAdapter(dataAdapter);
                    FinProgressBar();
                }

                FinProgressBar();

            }

            @Override
            public void onFailure(Call<List<Owner>> call, Throwable t) {
                Toast.makeText(getBaseContext(),"ERROR: "+ t.getLocalizedMessage() ,Toast.LENGTH_SHORT).show();

            }
        });

    }

}

